//Mathematical  Operations (-,*,/,%)
//Subtraction

let numString1 = "5";
let numString2 = "6";
let num1 = 5;
let num2 = 6;
let num3 = 5.5;
let num4 = .5;

console.log(num1-num3);//proper mathematical operation
console.log(num3-num4);//proper mathematical operation
console.log(numString1-num2);//the string was forced to become a number
console.log(numString2-num2);//the string was forced to become a number
console.log(numString1-numString2);//numeric strings will not concatenate and instead will be forcibly changed its type and subtract properly

let sample2 = "Juan Dela Cruz"
console.log(sample2-numString1);//NaN - results in not a number. When trying to perform subtraction between alphanumerical string and numeric string, the result is NaN

//Multiplication
console.log(num1*num2);
console.log(numString1*num1);
console.log(numString1*numString2);
console.log(sample2*5);

let product1 = num1*num2;
let product2 = numString1*num1;
let product3 = numString1*numString2;
console.log(product1);

//Division
console.log(product1/num2);
console.log(product2/5);
console.log(numString2/numString1);
console.log(numString2%numString1);

// division/multiplication by 0
console.log(product2 * 0);
console.log(product3 / 0);
//Division by 0 is not accurately and should not be done, it results to infinity

// % Modulo- remainder of a division operation
console.log(product2%num2);
console.log(product3%product2);
console.log(num1%num2);
console.log(num1%num1);

//Boolean (true or false)
/*
Boolean is usually used for logic operations or if-else condition
When creating a variable which will contain a boolean, the variable name is a usually a yes or no question
*/

let isAdmin = true;
let isMarried = false;
let isMVP = true;

//you can also concatenate strings + boolean
console.log("Is she married? " + isMarried);
console.log("Is he the MVP? "+ isMVP);
console.log(`Is he the the current Admin? ` + isAdmin);

//Arrays
/*
arrays are a special kind of data type to store multiple values
can actually store data with different types BUT as the best practice, arrays are used to contain multiple values of the SAME data type
values in a narrats are spearated by commas 
An Array is created with an array literal = []
*/

let array1 = ["Goku","Picolo","Gohan","Vegeta"];
console.log(array1);
let array2 = ["One Punch Man", true, 500, "Saitama"];
console.log(array2);

let grades = [98.7,92.1,90.2,94.6];
console.log(grades);
//Array are better thought of as group data


//Objects
/*
Objects are another special kind of data ytype used to mimic the real world
used to create complex data that contain pieces of information that are very relevant to each other

each data/value are paired with a key
each field is called a property
each field is separated comma

syntax:
	let/const objectName = {
		propertyA:value,
		propertyB:value
	}
*/

let person ={
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact:["+639154847451", "8123 4567"],
	address:{
		houseNumber:'345',
		street:'Diamond',
		city:'Manila'
	}
};

console.log(person);

/*
Mini-Activity
Create a variable with a group of data
	- The group of data should contain names from your favorite band/idol

Create a variable which contain multiple values of differing types and describes a single perosn.
	- This data type should be able to contain multiple key value pairs:
		firstName:<value>
		lastName:<value>
		isDeveloper:<value>
		hasPortfolioL<value>
		age:<value>
		contact:<value>(2 contact)
		address:
			houseNumber:<value>
			street:<value>
			city:<value>

*/

//Mini Activity
let imagineDragons = ["Dan Reynolds","Wayne Sermon","Ben McKee","Daniel Platzman"];
console.log(imagineDragons);

let myself ={
	firstName: 'Cyrus',
	lastName: 'Katigbac',
	isDeveloper:false,
	hasPortfolio: false,
	age: 19,
	contact:["+639010501404", "7890 1234"],
	address:{
		houseNumber:'1234',
		street:'Streeet',
		city:'Citty'
	}
};

console.log(myself);

//Undefine vs Null
//Null is explicit absence of data/value. This is done to project that a variable contains nothing over undefined merely means there is no data in the variable BECAUSE the variable has not been assigned an initial value.

let sampleNull = null;


//Undefined is a representation that a variable has been declated but it was not assigned an initial values

let sampleUndefined;

console.log(sampleNull);
console.log(sampleUndefined);

//certain processes in programming explicitly return null to indicate that the task resulted to nothing

let foundResult = null;
//for undefined, this is normally caused by developers creativng variables that have no value or data/assosciated with them
//This is when a variable does exist but its value is still unknown

let person2={
	name:"Peter",
	age:"35"
}
//because person2 does exist but the property isAdmin does not exist.
console.log(person2.isAdmin);

/*
Functions
- in JavaScript are line/block of codes that tell our device/application to perform a certain task when called/invoked
- are reusable pieces of code with instructions which used over and over again just as long as we can call/invoke them

Syntax:
	function functionName(){
		code block
			- the block of code that will be executed once the function has been run/called/invoked

	}

*/

//Declaring function
function printName(){
	console.log("My name is Cy");
};

//invoking/calling of function
printName();
printName();
printName();

function showSum(){
	console.log(25+6);
};

showSum();
// Do not create functions with the same name

/*
Parameters and Arguments
"name" is called a parameter
	a parameter acts a named variables/container that exists ONLY inside of the function. This is used as to store  information/ to act as a stand-in or the containe the value passed into the function as an argument.
*/

function printName(name){
	console.log(`My name is ${name}`);
};

//When a function is invoked and data is passed, we call the data as argument
//In this invocation, "V" is an argument passed into our peintName function ans is represented by the"name" parameter
//Data passed into the function : argument
//representaion of the argument within the function : parameter

printName("V");

function displayNum(number){
	alert(number);
};

displayNum(1000);
/*
Mini Activity:
Create a function which will be able to show message in the console.
	The message should be able to be passed into the function via an argument,

		Sample Message:
		"JavaScript is Fun"
		"C++ is Fun"

		argument: programming language

*/

//Mini Activity
function fun(){
	console.log("JavaScript is Fun");
};


function fun(course){
	console.log(`${course} is Fun`);
};

fun("JavaScript");
fun("C++");
fun("Programming Language");

function displayFullName(firstName,mi,lastName,age){
	console.log(`${firstName} ${mi} ${lastName} ${age}`);
};

displayFullName("Seok-jin","P","Kim","29");

//return keyword
	//The "return" statement allows the output of a function to be passed to the line/block of code theat invoked/called the function
	//any line/block of code that comes agter the returtn statement is ignoted beacuse it ends the function execution

	function createFullName(firstName,middleName,lastName){
		return `${firstName} ${middleName} ${lastName}`
		console.log("I will no longer run because the function's value/result has been returned")

	};

	let fullName1 = createFullName("Jungkook",  "BTS", "Jeon");
	let fullName2 = displayFullName("Tom", "Mapother", "Cruise");
	let fullName3 = createFullName("Tae-hyung", "BTS", "Kim")
	console.log(fullName1);
	console.log(fullName2);
	console.log(fullName3);